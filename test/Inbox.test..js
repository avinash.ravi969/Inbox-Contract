const assert = require('assert');
const ganache = require('ganache-cli');
//const provider = ganache.provider();
// web3 contructor function
const Web3 = require('web3');

// creating instance of web3 and connection with local network Ganache
const web3 = new Web3(ganache.provider());
// import interface and bytecode from compile.js file
const { interface, bytecode } = require('./../compile');

let accounts;
let inbox;
const INITIAL_MESSAGE = 'Hi! There!!';

beforeEach(async () => {

    // Get All Accounts
    // All function in web3 are async and returns a promise
    accounts = await web3.eth.getAccounts();
    // web3.eth.getAccounts()
    //     .then(fetchedAccounts => {
    //         console.log(fetchedAccounts);
    //     });

    // Use one of those accounts to deploy contract
    inbox = await new web3.eth.Contract(JSON.parse(interface))
        .deploy({ 
            data: bytecode, 
            arguments: ['Hi']
        })
        .send({ from: accounts[0], gas: '1000000'});

   // inbox.setProvider(provider);
});

describe('Inbox', () => {
    it('deploys a contract', () => {
        assert.ok(inbox.options.address);
    });

    it('has a default message', async () => {
        const message = await inbox.methods.message().call();
        assert.equal(message, 'Hi');
    });

    it('can set', async () => {
      await inbox.methods.setMessage('bye').send({ from: accounts[0] });
      const message = await inbox.methods.message().call();
      assert.equal(message, 'bye');
    });
});